/*
 * d2c
 *
 * Device to cloud API
 *
 * API version: 0.1
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package easyplatform

type ApiAssistantRequestRequest struct {

	Command string `json:"command,omitempty"`

	OriginalUtterance string `json:"original_utterance,omitempty"`

	Type_ string `json:"type,omitempty"`

	Markup *ApiAssistantRequestRequestMarkup `json:"markup,omitempty"`

	Payload *interface{} `json:"payload,omitempty"`

	Nlu *ApiAssistantRequestRequestNlu `json:"nlu,omitempty"`

	Session *ApiAssistantRequestRequestSession `json:"session,omitempty"`

	Version string `json:"version,omitempty"`
}
