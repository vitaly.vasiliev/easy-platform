/*
 * d2c
 *
 * Device to cloud API
 *
 * API version: 0.1
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package main

import (
	//"crypto/tls"
	"log"
	"net/http"

	// WARNING!
	// Change this to a fully-qualified import path
	// once you place this file into your project.
	// For example,
	//
	//    sw "github.com/myname/myrepo/go"
	//
	sw "./go"
)

func main() {

	log.Printf("Server started")

	router := sw.NewRouter()

	go http.ListenAndServe(":3030", http.FileServer(http.Dir("/images")))

	go http.ListenAndServe(":80", router)

	log.Fatal(http.ListenAndServeTLS(":443", "certs/fullchain.pem", "certs/privkey.pem", router))
}
